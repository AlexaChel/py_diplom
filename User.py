import json
from Request import Request


class User:
    def __init__(self, token, id):
        self.token = token
        self.id = id
        self.groups = []
        self.friends = []
        self.info = {}

    def get_params(self):
        params = {
            'access_token': self.token,
            'v': '5.52'
        }

        if self.id:
            params['user_ids'] = self.id

        return params

    def get_info(self):
        params = self.get_params()

        with Request('get', 'users.get', params) as response:
            self.info = response
            return response

    def get_friends(self):
        params = self.get_params()
        del params['user_ids']

        if str(self.id).isdigit():
            params['user_id'] = self.id
        else:
            params['user_id'] = self.get_info()[0]['id']

        with Request('get', 'friends.get', params) as response:
            friend_ids = response['items']
            friends = list()

            for friend_id in friend_ids:
                friends.append(User(self.token, friend_id))

            self.friends = friends

    def get_groups(self):
        params = self.get_params()
        params['count'] = 1000
        del params['user_ids']

        if str(self.id).isdigit():
            params['user_id'] = self.id
        else:
            params['user_id'] = self.get_info()[0]['id']

        with Request('get', 'groups.get', params) as response:
            group_ids = response['items']

            self.groups = group_ids
            return group_ids

    def get_group(self, group_ids):
        params = self.get_params()
        params['extended'] = 1
        params['fields'] = 'members_count'
        params['group_ids'] = str(group_ids).strip('[]').strip()

        with Request('get', 'groups.getById', params) as response:
            return response

    def write_file(self, groups):
        with open('groups.json', 'w', encoding='utf-8') as f:
            data = []
            for group in groups:
                data.append({
                    'name': group['name'],
                    'gid': group['id'],
                    'members_count': group['members_count']
                })

            json.dump(data, f, ensure_ascii=False)

    def get_unique_groups_user(self):
        if not self.friends:
            self.get_friends()

        if not self.groups:
            self.get_groups()

        friends = self.friends
        group_ids = set(self.groups)

        for key, friend in enumerate(friends):
            groups_friend = friend.get_groups()

            if groups_friend:
                groups_friend = set(groups_friend)
                group_ids.difference_update(groups_friend)

            print('Осталось обработать -', len(friends) - (key + 1), 'друзей')

        groups = self.get_group(list(group_ids))
        self.write_file(groups)
