import time
import requests


class Request:
    def __init__(self, method, url, params):
        self.method = method
        self.url = url
        self.params = params

    def __enter__(self):
        response = requests.get(
            f'https://api.vk.com/method/{self.url}',
            self.params
        )

        try:
            self.response = response.json()['response']
            return self.response
        except:
            error_code = response.json()['error']['error_code']

            print(error_code)

            if error_code == 6:
                time.sleep(0.3)

            return {'items': []}

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass
